﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
//using RemoteValiDationInMVC.Models;

namespace RemoteValiDationInMVC.Controllers
{
    public class RegisterController : Controller
    {
        // GET: Register  
        [HttpGet]
        public ActionResult SignUp()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SignUp(RegisterModel ObjModel)
        {
            if (ModelState.IsValid)
            {

                return View();

            }
            else
            {

                return View();

            }

        }
        [HttpPost]
        public JsonResult IsAlreadySigned(string UserEmailId)
        {

            return Json(IsUserAvailable(UserEmailId));

        }
        public bool IsUserAvailable(string EmailId)
        {
            // Assume these details coming from database  
            List<RegisterModel> RegisterUsers = new List<RegisterModel>()
        {

            new RegisterModel {UserEmailId="vithal.wadje@abc.com" ,PassWord="compilemode",Designation="SE"},
            new RegisterModel {UserEmailId="Sudhir@abc.com" ,PassWord="abc123",Designation="Software Dev"}

        };
            var RegEmailId = (from u in RegisterUsers
                              where u.UserEmailId.ToUpper() == EmailId.ToUpper()
                              select new { EmailId }).FirstOrDefault();

            bool status;
            if (RegEmailId != null)
            {
                //Already registered  
                status = false;
            }
            else
            {
                //Available to use  
                status = true;
            }

            return status;
        }
    }

}